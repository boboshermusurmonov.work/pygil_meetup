import threading


class Number:

    def __init__(self, value: int = 0):
        self.value = value

    def __str__(self) -> str:
        return str(self.value)

    def increment(self) -> None:
        self.value = self.value + 1

    def decrement(self) -> None:
        self.value = self.value - 1


def do_nothing(num: Number):
    for i in range(1_000_000):
        num.increment()
        num.decrement()


def main():
    num = Number(value=0)
    t1 = threading.Thread(target=do_nothing, args=(num,))
    t2 = threading.Thread(target=do_nothing, args=(num,))
    t1.start()
    t2.start()
    t1.join()
    t2.join()
    print(num)


if __name__ == "__main__":
    main()
