"""
For demonstrating performance differences between multithreading and multiprocessing.
To make code more understandable for the viewer, I had to refuse DRY principle.
"""

import threading
import multiprocessing
from utils import timer as local_timer


def countdown(n: int) -> None:
    while n > 0:
        n -= 1


def run_in_threads(n: int, num_threads: int) -> None:
    timer = local_timer.Timer()
    timer.start()
    threads = []
    for i in range(num_threads):
        th = threading.Thread(target=countdown, args=(n//num_threads,))
        th.start()
        threads.append(th)
    for th in threads:
        th.join()
    timer.finish()
    print(f"Time spent for running in {num_threads} threads: {timer.get_total_time()}")


def run_in_processes(n: int, num_processes: int) -> None:
    timer = local_timer.Timer()
    timer.start()
    processes = []
    for i in range(num_processes):
        pr = multiprocessing.Process(target=countdown, args=(n//num_processes,))
        pr.start()
        processes.append(pr)
    for pr in processes:
        pr.join()
    timer.finish()
    print(f"Time spent for running in {num_processes} processes: {timer.get_total_time()}")


def main():
    COUNT = 50_000_000

    run_in_threads(n=COUNT, num_threads=1)
    run_in_threads(n=COUNT, num_threads=2)
    run_in_threads(n=COUNT, num_threads=4)

    run_in_processes(n=COUNT, num_processes=1)
    run_in_processes(n=COUNT, num_processes=2)
    run_in_processes(n=COUNT, num_processes=4)


if __name__ == "__main__":
    main()
