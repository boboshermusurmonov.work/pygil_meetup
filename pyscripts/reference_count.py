

def main():
    a, b, c = 4, 5, 5  # Boshlang'ich qiymatlar
    print(a, b, c)
    
    c = 6  # 6 qiymatli yangi obyekt ochib, c ning bog'lovchisini 6 ga o'tkazamiz.
    print(a, b, c)
    
    a = 5  # a ning 4 da turgan bog'lovchisini 5 ga o'tkazamiz.
    print(a, b, c)
    

if __name__ == "__main__":
    main()
