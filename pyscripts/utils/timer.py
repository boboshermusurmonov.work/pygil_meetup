import time
import functools


class Timer:
    """
    A simple timer to measure execution time
    of a piece of code.
    """

    def _set_total_time(self):
        self.total = self.finish - self.start

    def get_total_time(self):
        return self.total

    def start(self) -> None:
        self.start = time.perf_counter()

    def finish(self) -> None:
        self.finish = time.perf_counter()
        self._set_total_time()

    def wrap(self, func):
        """
        A decorator to measure execution time
        of a function.
        """
        @functools.wraps(func)
        def inner(*args, **kwargs):
            self.start()
            func(*args, **kwargs)
            self.finish()
        return inner
