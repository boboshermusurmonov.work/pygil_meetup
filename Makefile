race:
	python pyscripts/race_condition.py

refc:
	python pyscripts/reference_count.py

conc:
	python pyscripts/concurrency.py
